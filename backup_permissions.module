<?php

/**
 * @file
 * Module that provide functionality to store permission states.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_alter() for user_admin_permissions().
 */
function backup_permissions_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $config = \Drupal::config('backup_permissions.settings');
  if (($form_id == 'user_admin_permissions') && ($config->get('auto_backup_config') == 1)) {
    array_unshift($form['#submit'], 'backup_permissions_save_permissions_state');
  }
}

/**
 * Form submission handler for creating backup before updating permissions.
 *
 * @see user_admin_permissions()
 */
function backup_permissions_save_permissions_state(&$form, FormStateInterface $form_state) {
  $selected_roles = user_role_names();
  // Adding default title.
  $date = \Drupal::service('date.formatter')->format(time(), 'custom', 'M-d');
  $title = 'Backup-' . $date;
  // Getting permissions of selected roles.
  $backup = backup_permissions_get_data($selected_roles);
  // Serialising permissions state and storing in database.
  $backup = serialize($backup);
  // Save the submitted entry.
  $entry = [
    'title' => $title,
    'created' => time(),
    'backup' => $backup,
  ];
  $db = \Drupal::database();
  $db->insert('backup_permissions')
    ->fields($entry)
    ->execute();
}

/**
 * Returns associative array of permission data.
 *
 * @param array $seleted_roles
 *   Array of roles to get permissions.
 *
 * @return array
 *   Associative array of permissions and roles.
 */
function backup_permissions_get_data(array $seleted_roles) {
  $roles = user_roles();
  $perm_role = [];
  $permission_handler = \Drupal::service('user.permissions');
  $permissions = $permission_handler->getPermissions();

  foreach ($permissions as $perm => $perm_item) {
    // Fill in default values for the permission.
    $row = [
      'name' => $perm,
    ];

    // Get the roles object data.
    foreach ($seleted_roles as $rid => $value) {
      $role = $roles[$rid];
      $name = $role->id();
      $perm_role[$rid] = $name;

      // Builds arrays for checked boxes for each role.
      if ($role->hasPermission($perm) || $role->isAdmin()) {
        $row[$name] = "Yes";
      }
      else {
        $row[$name] = "No";
      }
    }
    $rows[] = $row;
  }
  $data = [];
  $data['roles'] = $perm_role;
  $data['permissions'] = $rows;

  return $data;
}
