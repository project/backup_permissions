<?php

namespace Drupal\backup_permissions\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\backup_permissions\BackupPermissionsStorageTrait;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Session\AccountInterface;

/**
 * Form to list out available backups.
 */
class BackupPermissionsListForm extends FormBase {

  use BackupPermissionsStorageTrait;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The tempstore object.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Constructs a BackupPermissionsCreateForm object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The temp factory service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(
    DateFormatterInterface $date_formatter,
    PrivateTempStoreFactory $temp_store_factory,
    AccountInterface $current_user,
    Renderer $renderer
  ) {
    $this->dateFormatter = $date_formatter;
    $this->tempStore = $temp_store_factory->get('backup_permissions');
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('tempstore.private'),
      $container->get('current_user'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'backup_permissions_list_form';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $header = [
      $this->t('Title'),
      $this->t('Created'),
      '',
    ];
    $rows = [];

    $results = $this->getBackupList();
    foreach ($results as $result) {
      $drop_button = [
        '#type' => 'dropbutton',
        '#links' => [
          'reset' => [
            'title' => $this->t('Reset'),
            'url' => Url::fromRoute('backup_permissions.reset', ['bid' => $result->id]),
          ],
          'download' => [
            'title' => $this->t('Download'),
            'url' => Url::fromRoute('backup_permissions.download', ['bid' => $result->id]),
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('backup_permissions.delete', ['bid' => $result->id]),
          ],
        ],
      ];
      $rows[$result->id] = [
        $result->title,
        $this->dateFormatter->format($result->created, 'long'),
        [
          '#markup' => $this->renderer->render($drop_button),
        ],
      ];
    }
    $form['automatic_backup'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Automatic backup'),
    ];
    $form['automatic_backup']['backup_permissions_auto_backup_config'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically backup permission every-time permissions are updated.'),
      '#default_value' => $this->configFactory()
        ->get('backup_permissions.settings')
        ->get('auto_backup_config'),
    ];
    $form['automatic_backup']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Configuration'),
      '#name' => 'save_configuration',
    ];
    $form['options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Update options'),
      '#attributes' => ['class' => ['container-inline']],
    ];
    $options = ['delete' => 'Delete the selected backups'];

    $form['options']['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#title_display' => 'invisible',
      '#options' => $options,
      '#default_value' => 'delete',
    ];

    $form['options']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#name' => 'update',
    ];

    $form['backups'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t('No backups found'),
    ];
    $form['pager'] = [
      '#type' => 'pager',
      '#weight' => 10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#name'] == 'update') {
      $backups = array_filter($form_state->getValue('backups'));
      if (count($backups) == 0) {
        $form_state->setErrorByName('', $this->t('No backups selected.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#name'] == 'update') {
      $this->tempStore->set($this->currentUser->id(), array_filter($form_state->getValue('backups')));
      $form_state->setRedirect('backup_permissions.multiple_delete_confirm');
    }
    else {
      $config = $this->configFactory()
        ->getEditable('backup_permissions.settings');

      $config->set('auto_backup_config', $form_state->getValue('backup_permissions_auto_backup_config'))
        ->save();
    }
  }

}
